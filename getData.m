function [ dates, logReturns, maxVol ] = getData()

if (~exist('dates') & ~exist('logReturns')) % Only load once
  [dates, logReturns] = loadHistoricalData;
  logReturns = logReturns(:,1);
end

maxVol = 0.5;


end

