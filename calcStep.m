function [stepSize] = calcStepSize(c, R, w, beta, dw)
%CALCSTEP Summary of this function goes here
%   Detailed explanation goes here

t = (c - R)' * w + R;
n = (c - R)' * dw;
temp = (n < 0) .* (t ./ - n);
I = find(temp);

stepSize = min(beta * min(temp(I)), 1);

end

