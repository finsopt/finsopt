function [solutions, step, normgrad] = initAllSolutions(logReturns, R, volatilities, initialHolding, stepLimit, gamma, maxIterations )
%INITALLSOLUTIONS Summary of this function goes here
%   Detailed explanation goes here
solutions = zeros(size(volatilities));
step = zeros(size(volatilities));
normgrad = zeros(size(volatilities));
nVolatilities = length(volatilities);
for i = 1:nVolatilities
    fprintf('Optimization progress: %i/100\n', int16(100 * i / nVolatilities));
    [solutions(i), step(i), normgrad(i)] = solver(logReturns * volatilities(i), initialHolding(1:end-1), R, stepLimit, gamma, maxIterations);
end

end

