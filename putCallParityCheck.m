function [ LH, RH ] = putCallParityCheck(callPrices, putPrices, maturities, currentPrice, strikes, interestRate, dt)
%PUTCALLPARITYCHECK Summary of this function goes here
%   Detailed explanation goes here
LH = currentPrice + putPrices;
RH = callPrices + strikes .* exp(interestRate .* maturities *dt);

end

