function [pastVolatilities] = calculatePastVolatilities(pastReturns)
numberOfValues = length(pastReturns);
pastVolatilities = zeros(numberOfValues,1);

LAMBDA = 0.94;
meanPastReturn = mean(pastReturns);

pastReturns = flipud(pastReturns);

% ----------------------------------------
% Calculate the starting value for EWMA

meanPastReturn

pastVolatilities(1) = sum((pastReturns - meanPastReturn).^2)/(numberOfValues-1);

% -----------------------------------------
% Calculate matrix of volatilites with EWMA

for i = 1:numberOfValues-1
    pastVolatilities(i+1) = LAMBDA*pastVolatilities(i)+(1-LAMBDA)*pastReturns(i)^2;
end


%-----------------------------------------
% Converting the variance to volatility
% Converting daily volatility to yearly
pastVolatilities = sqrt(pastVolatilities);

pastVolatilities = flipud(pastVolatilities);
end
