function [normReturnsEwma normReturnsGarch] = calculateNormReturns(values,garchStart)
pastReturns = calculatePastReturns(values)

pastVolatilitiesEwma = calculatePastVolatilities(pastReturns)
pastVolatilitiesGarch = calculatePastVolatilitiesGARCH(pastReturns,garchStart);

normReturnsEwma = pastReturns./pastVolatilitiesEwma;
normReturnsGarch = pastReturns./pastVolatilitiesGarch;
end