function [dir] = calcDirection(w, c, R, gamma)
%FPRIME Summary of this function goes here
%   Detailed explanation goes here
nAssets = size(c, 1);
nSamples = size(c, 2);

p = 1 / nSamples;
z = (c - R)' * w + R;

if gamma == 0
    Uprim = 1 ./ z;
    Ubis = -1 ./ z.^2;
elseif gamma <= 1
    Uprim = z .^ (gamma - 1);
    Ubis = (gamma - 1) * z .^ (gamma - 2);
end

fgrad = zeros(size(c,1), 1);
for i = 1:size(c,2)
    fgrad = fgrad + p * Uprim(i) * (c(:,i) - R);
end

fhess = zeros(nAssets);
for i =  1:size(c,2)
    fhess = fhess + p * Ubis(i) * (c(:,i) - R) * (c(:,i) - R)';
end

dir = -inv(fhess) * fgrad;

end

