function [pastReturns] = calculatePastReturns(values)
numberOfValues = length(values);
pastReturns = zeros(numberOfValues-1,1);
for i = 1:numberOfValues-1
    pastReturns(i) = log(values(i)/values(i+1));
end
end