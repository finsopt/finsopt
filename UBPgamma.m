function [Price] = UBPgamma(estW, estIndex, strike, currPrice, maturities, maturityIndex, gamma, nSamples, interestRate, dt, PC)
% runtime �r indexet i estW och estIndex
if(PC == 1) % Call
    PT = max(0,estIndex(:, maturityIndex) * currPrice - strike);
else % Put
    PT = max(0,strike - estIndex(:, maturityIndex) * currPrice);
end
%for j = 1:length(estIndex)
%PT(j)=max(0,estIndex(j,runtime)*currPrice-Strike);
%end
if gamma == 0 
    Price = mean(PT ./ estW(:, maturityIndex)); % ty wt = 1
else
    dU = estW(:, maturityIndex).^(gamma - 1);
    upper=(1 / nSamples) * sum(dU .* PT); 
    lower=(1 / nSamples) * sum(dU) * exp(interestRate * maturities(maturityIndex) * dt); % eftersom avkastningen r�knas p� handelsdagar
    Price = upper / lower;
end

end

