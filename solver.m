function [ w, step, normgrad] = solver(scenarioReturns, w0, R, stepLimit, gamma, maxIterations)
c = exp(scenarioReturns');
w = w0;
normgrad = Inf;
step = 0;
beta = 0.99;
while normgrad > stepLimit && step < maxIterations
    direction = calcDirection(w, c, R, gamma);
    stepSize = calcStep(c, R, w, beta, direction);
    w = w + stepSize * direction;
    step = step + 1;
    normgrad = norm(direction);
end