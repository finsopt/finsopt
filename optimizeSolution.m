function [volatilities, solutions, step, normgrad] = optimizeSolution(logReturns, nVolatilities, pastVolatilities, gamma, interestRate, dt)

maxVol = 2 * max(pastVolatilities);
nAssets = size(logReturns, 2);

R = exp(interestRate * dt);
stepLimit = 10^-10;
maxIterations = 100;

dv = maxVol / nVolatilities;
volatilities = [dv:dv:maxVol];

initialHolding = zeros(1, nAssets + 1);
initialHolding(1, nAssets + 1) = 1; % Only cash

[solutions, step, normgrad] = initAllSolutions(logReturns, R, volatilities, initialHolding, stepLimit, gamma, maxIterations);

%plot av funktionen vol -> soulution
%volatilitiesY = volatilities / sqrt(dt);
%plot(volatilitiesY, solutions);

% Test av interpolation ska ta bort sen.
%v = dv + (maxVol-dv).*rand(3000000 ,1);
%tic
%s = interp1(volatilities, solutions, v);
%toc
end