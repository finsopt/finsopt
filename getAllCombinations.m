function [options] = getAllCombinations(options)
%GETALLCOMBINATIONS Summary of this function goes here
%   Detailed explanation goes here
minT = min(options(:,1));
maxT = max(options(:,1));
stepT = (maxT - minT) / 100;
T = minT:stepT:maxT;

minK = min(options(:,2));
maxK = max(options(:,2));
stepK = (maxK - minK) / 100;
K = minK:stepK:maxK;

options = zeros(length(K) * length(T), 2);

i = 1;
for t = T
    for k = K
        options(i,1) = t;
        options(i,2) = k;
        i = i + 1;
    end
end

end

