addpath ('indexData');
load('SP500.mat');
load('DAX.mat');
load('OMXS.mat');
load('FCHI.mat');

% Help variables
Call = 1;
Put = 0;
dt = 1/240;
nSamples = 100000;
nOptVolatilities = 500;
rand('state', 0);

% Set all interest rates
SP500InterestRate = 0.000177496;
DAXInterestRate = -0.002070536;
OMXSInterestRate = 0.000449975;
FCHIInterestRate = -0.000060000;

% Get S0 for each index
SP500S0 = SP500values(1);
OMXSS0 = OMXSvalues(1);
DAXS0 = DAXvalues(1);
FCHIS0 = FCHIvalues(1);

% Prepare data for each index
[SP500priceCall, SP500pricePut, SP500priceBSCall, SP500priceBSPut, SP500CallGrid, SP500PutGrid] = priceDerivatives(SP500pastReturns, SP500vols, SP500Call, SP500Put, SP500S0, SP500InterestRate, nSamples, nOptVolatilities, dt);
[OMXSpriceCall, OMXSpricePut, OMXSpriceBSCall, OMXSpriceBSPut, OMXSCallGrid, OMXSPutGrid] = priceDerivatives(OMXSpastReturns, OMXSvols, OMXSCall, OMXSPut, OMXSS0, OMXSInterestRate, nSamples, nOptVolatilities, dt);
[DAXpriceCall, DAXpricePut, DAXpriceBSCall, DAXpriceBSPut, DAXCallGrid, DAXPutGrid] = priceDerivatives(DAXpastReturns, DAXvols, DAXCall, DAXPut, DAXS0, DAXInterestRate, nSamples, nOptVolatilities, dt);
[FCHIpriceCall, FCHIpricePut, FCHIpriceBSCall, FCHIpriceBSPut, FCHICallGrid, FCHIPutGrid] = priceDerivatives(FCHIpastReturns, FCHIvols, FCHICall, FCHIPut, FCHIS0, FCHIInterestRate, nSamples, nOptVolatilities, dt);

% Calculate all implied volatilities
% [SP500CallT, SP500CallM, SP500CallV, SP500CallP, SP500CallF] = volSurf(SP500InterestRate, SP500S0, SP500CallGrid(:,1) * dt, SP500CallGrid(:,2), SP500priceCall, Call);
% [SP500PutT, SP500PutM, SP500PutV, SP500PutP, SP500PutF] = volSurf(SP500InterestRate, SP500S0, SP500PutGrid(:,1) * dt, SP500PutGrid(:,2), SP500pricePut, Put);
% [OMXSCallT, OMXSCallM, OMXSCallV, OMXSCallP, OMXSCallF] = volSurf(OMXSInterestRate, OMXSS0, OMXSCallGrid(:,1) * dt, OMXSCallGrid(:,2), OMXSpriceCall, Call);
% [OMXSPutT, OMXSPutM, OMXSPutV, OMXSPutP, OMXSPutF] = volSurf(OMXSInterestRate, OMXSS0, OMXSPutGrid(:,1) * dt, OMXSPutGrid(:,2), OMXSpricePut, Put);

% Plot implied volatility surfaces
% plotVolSurf(SP500CallT, SP500CallM, SP500CallV);
% plotVolSurf(SP500PutT, SP500PutM, SP500PutV);
% 
% plotVolSurf(OMXSCallT, OMXSCallM, OMXSCallV);
% plotVolSurf(OMXSPutT, OMXSPutM, OMXSPutV);

% Test functions
% plotBSCall(2075.37, M(Y)*2075.37, T(X), SP500InterestRate, P(Y,X));