function [pastVolatilities] = calculatePastVolatilitiesGARCH(pastReturns,garchStart)
% V�rden utr�knade med Excel
w = garchStart(1);
alpha = garchStart(2);
beta = garchStart(3);

numberOfValues = length(pastReturns);
pastVolatilities = zeros(numberOfValues,1);

meanPastReturn = mean(pastReturns)

pastReturns = flipud(pastReturns);

% ----------------------------------------
% Calculate the starting value for EWMA

pastVolatilities(1) = sum((pastReturns - meanPastReturn).^2)/(numberOfValues-1);

% -----------------------------------------
% Calculate matrix of volatilites with EWMA

for i = 1:numberOfValues-1
    pastVolatilities(i+1) = w + beta*pastVolatilities(i)+alpha*pastReturns(i)^2;
end


%-----------------------------------------
% Converting the variance to volatility
% Converting daily volatility to yearly
pastVolatilities = sqrt(pastVolatilities);

pastVolatilities = flipud(pastVolatilities);
end
