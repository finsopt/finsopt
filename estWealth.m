function [WtHistory, indexHistory, AVGWtHistory] = estWealth(maturities, volatilities, solutions, nSamples, initialVolatility, logReturns, interestRate, dt, lambda)
%ESTWEALTH Summary of this function goes here
%   Detailed explanation goes here

% denna variant klarar flera olika nyttofunktioner
% UBPgamma(logReturns,1400,1300,20,0.02,0.01,0,volatilities,solutions)
% pr�va med detta anro

%indexReturn blir extremt volatil kolla hur allt �r utr�knat kan troligtvis
%vara att jag skalar logReturns fel, ev kolla hur randsample fungerar
alpha = interp1(volatilities, solutions, initialVolatility);
beta = 1 - alpha;
Rf = exp(interestRate * dt);

WtHistory=zeros(nSamples,length(maturities));
indexHistory=zeros(nSamples,length(maturities));

maxMaturity = max(maturities);

for j = 1:nSamples
    fprintf('WT estimation progress: %i/100\n', int16(100 * j / nSamples));
    volHistory = zeros(maxMaturity, 1);
    maturityIndex = 1;
    Wt = 1;
    indexReturn = 1;
    currentVolatility = initialVolatility;
    alpha = interp1(volatilities, solutions, currentVolatility);
    beta = 1 - alpha;
    for i = 1:maxMaturity
        % Calculate the return for the index for one day
        R_I = exp(currentVolatility * logReturns(randsample(length(logReturns),1)));
        % Calculate the cumulative portfolios wealth
        Wt = Wt * (alpha * R_I +  beta * Rf);
        % Calculate the comulative index return
        indexReturn = indexReturn * R_I;
        % Update the volatility
        currentVolatility = sqrt(lambda * currentVolatility^2 + (1 - lambda) * (R_I - 1)^2);
        % Update aplha and beta
        alpha = interp1(volatilities,solutions,currentVolatility);
        volHistory(i) = currentVolatility / sqrt(dt);
        if isnan(alpha)
            display('Fel');
            plot(volHistory);
        end
        beta = 1 - alpha;
        % Check if we need to save the data
        if i == maturities(maturityIndex)
            WtHistory(j,maturityIndex) = Wt;
            indexHistory(j,maturityIndex) = indexReturn;
            maturityIndex = maturityIndex + 1;
        end
    end
end
end

