function P = BSPut( vol, S0, K, T, r )
%Black and Scholes pricing function for put
%   Assumption: Non-dividend paying stock
F = S0.'*exp(r.*T);
d1=(log(F./K)+(r+(vol.*vol)./2)*T)./(vol.*sqrt(T));
d2 = d1 - vol*sqrt(T);
P = exp(-r.*T).*(K.*normcdf(-d2)) - S0.*normcdf(-d1);
end

