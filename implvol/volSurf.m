function [Tt, Mm, impVolCalc, pricegrid, FVAL] = volSurf(r, S0, T, K, P, putCall)% Calculating implied volatility surface of a set of option prices
% Based on the VolSurf model developed by Radolphe Sitter and Yi Cao%
%-----------------------------------------------------------------%
%Variable declaration

%------------------------------------------------------------------%
%Read option prices from where they are stored
%Need to be changed in order to fit with rest of program
%data = xlsread('SPX.xls','calc');

%Get variables from data
% T=data(3:end,2);          % maturity
% K=data(3:end,3);            % Strike price
% P=data(3:end,4);         % option prices

tic
%Calculate simulated prices and real prices
[impVolCalc, Kk, Tt, pricegrid, FVAL] = calcSurf(S0,r,T,K,P, putCall);
%surfReal = calcSurf(S0,r,T,K,P);

Mm = Kk./S0;

%Plot graphs retrieved from program

% 
% figure(2)
% surf(surfCalc.T,surfCalc.M,surfCalc.IV-surfCalc.IV)
% axis tight; grid on;
% title('Difference in volatility surface');
% xlabel('Time to maturity, T');
% ylabel('Moneyness');
% zlabel('Implied Volatility');

toc
end
