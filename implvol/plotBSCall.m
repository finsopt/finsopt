function [] = plotBSCall(S0, K, T, r, currentPrice)
%PLOTBSCALL Summary of this function goes here
%   Detailed explanation goes here
vol = 0:0.01:2;
P = zeros(length(vol),1);
for i = 1:length(vol)
    P(i) = BSCall(vol(i), S0, K, T, r)
end
plot(vol, [P, ones(length(vol)) * currentPrice]);
end

