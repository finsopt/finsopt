function v = vega(S0, T, K, r, vol)
%VEGA Summary of this function goes here
%   Calculate option vega

d1 = log(S0/K)+(r+((vol^2)/2))*T;
d1 = d1/(vol*sqrt(T));

v = S0*sqrt(T)*normpdf(d1);


end

