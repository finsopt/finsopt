function [ impvol, fval ] = impvolCalc(S0, K, P, T, r, putcall)
%IMPVOLCALC Summary of this function goes here
%   Detailed explanation goes here
if putcall == 1
    f = @(x) BSCall(x, S0, K, T, r);
else
    f = @(x) BSPut(x, S0, K, T, r);
end

maxIter = 1000;
tol = 1e-10;
diff = inf;
temp = 0;
i = 1;

maxBnd = 10;
minBnd = 0;
fmax = f(maxBnd);
fmin = f(minBnd);
while diff^2 > tol && i < maxIter
    midBnd = (maxBnd - minBnd) / 2 + minBnd;
    fmid = f(midBnd);
    diff = fmid - P;
%     if (temp == diff)
%         q = 1;
%         bnd = midBnd;
%         v = vega(S0, T, K, r, bnd);
%         while (v>1e-6) && q<20
%             bnd = ((bnd-minBnd)/2)+minBnd;
%             v = vega(S0, T, K, r, bnd);
%         end
%         minBnd = bnd;
%     else
%         temp = diff;
        if diff > 0
            maxBnd = midBnd;
        else
            minBnd = midBnd;
        end
%     end
    i = i + 1;
end
impvol = midBnd;
fval = f(impvol) - P;
% [impvol,FVAL,EXITFLAG,OUTPUT]= fminbnd(f, 0, 4, optimset('TolX',1e-15, 'MaxIter', 1000000, 'Display','on'));

end
