function d = func( vol, S0, K, T, r, CallPrice )
%This is the objective function for the optimization problem.
% Minimize difference (d) between "real" price and the calculated with
% regard to volatility. When the difference is small enough, vol is our
% impl. vol.
%   Detailed explanation goes here

C = BS(vol, S0, K, T, r);
d = CallPrice - C;
end

