function C = BSCall( vol, S0, K, T, r )
%Black and Scholes pricing function for call
%   Assumption: Non-dividend paying stock
F = S0.'*exp(r.*T);
d1=(log(F./K)+(r+(vol.*vol)./2)*T)./(vol.*sqrt(T));
d2 = d1 - vol*sqrt(T);
C =  S0.*normcdf(d1)-exp(-r.*T).*(K.*normcdf(d2));
end

