function [ImpliedVol, Kk, Tt, pricegrid, FVAL] = calcSurf( S0, r, T, K, price, putCall)

Tt = unique(T);
Kk = unique(K);

%Kk = unique(K(find(T == Tt(end))));
ImpliedVol = NaN(length(Kk),length(Tt));
pricegrid = NaN(length(Kk),length(Tt));
FVAL = NaN(length(Kk),length(Tt));
nSamples = length(Tt) * length(Kk);
iterator = 1;
for i = 1:length(Tt)
    for j = 1:length(Kk)
        q = find((Tt(i)==T) .* (Kk(j)==K));
        if (~isempty(q))
           [ImpliedVol(j,i), FVAL(j,i)] = impvolCalc(S0, Kk(j), price(q), Tt(i), r, putCall);
            pricegrid(j,i) = price(q);
        end
        fprintf('Surface progress: %i/100\n', int16(iterator / nSamples * 100));
        iterator = iterator + 1;
    end
end






end

